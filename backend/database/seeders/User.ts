import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from 'App/Models/User'

export default class UserSeeder extends BaseSeeder {
  public static developmentOnly = true

  public async run() {
    await User.createMany([
      {
        email: 'testpark@gmail.com',
        password: 'password',
        firstname: 'test',
        lastname: 'test',
      },
      {
        email: 'testparkadmin@gmail.com',
        password: 'password',
        firstname: 'test',
        lastname: 'test',
        role: 'ADMIN',
      },
      {
        email: 'flopark@gmail.com',
        password: 'password',
        firstname: 'florian',
        lastname: 'marques',
        role: 'ADMIN',
      },
    ])
  }
}
