export const state = () => ({
    email: "",
    token: ""
})

export const mutations = {  
    login (state, payload) {
        state.email = payload.email
        state.token = payload.token
    },
    reset (state) {
        state.email = ""
        state.token = ""
    },
}  